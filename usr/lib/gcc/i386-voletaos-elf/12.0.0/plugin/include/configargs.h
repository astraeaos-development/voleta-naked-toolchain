/* Generated automatically. */
static const char configuration_arguments[] = "../voleta-os-gcc/configure --target=i386-voletaos-elf --build=i386-voletaos-elf --prefix=/mnt/voleta-naked-toolchain/usr --disable-nls --enable-languages=c,c++ --without-headers";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "i386" }, { "arch", "i386" } };
